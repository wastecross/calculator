import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NumbersComponent } from './components/numbers/numbers.component';
import { BasicComponent } from './components/basic/basic.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { Basic2Component } from './components/basic2/basic2.component';
import { TrigonometryComponent } from './components/trigonometry/trigonometry.component';
import { DeveloperComponent } from './components/developer/developer.component';
import { ScientificComponent } from './components/scientific/scientific.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { HeaderComponent } from './components/home/header/header.component';

import { CalculatorService } from './services/calculator.service';

@NgModule({
  declarations: [
    AppComponent,
    NumbersComponent,
    BasicComponent,
    CalculatorComponent,
    Basic2Component,
    TrigonometryComponent,
    DeveloperComponent,
    ScientificComponent,
    NavbarComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ CalculatorService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
