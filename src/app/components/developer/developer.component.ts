import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.css']
})

export class DeveloperComponent {
  @Output() sendValueD = new EventEmitter();

  addToTextBox(symbol: string) {
    this.sendValueD.emit(symbol);
  }

}
