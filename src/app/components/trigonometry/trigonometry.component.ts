import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-trigonometry',
  templateUrl: './trigonometry.component.html',
  styleUrls: ['./trigonometry.component.css']
})
export class TrigonometryComponent {

  @Output() sendTrigonometry = new EventEmitter();

  addToTextBox(num: string) {
    this.sendTrigonometry.emit(num);
  }
}
