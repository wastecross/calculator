import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent {
  @Output() sendValue = new EventEmitter();

  addToTextBox(symbol: string) {
    this.sendValue.emit(symbol);
  }

}
