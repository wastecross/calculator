import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-scientific',
  templateUrl: './scientific.component.html',
  styleUrls: ['./scientific.component.css']
})
export class ScientificComponent {

  @Output() sendSci = new EventEmitter();

  addToTextBox(num: string) {
    this.sendSci.emit(num);
  }

}
