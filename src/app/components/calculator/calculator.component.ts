import { Component } from '@angular/core';
import { CalculatorService } from '../../services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css'],
  providers: [CalculatorService]
})

export class CalculatorComponent {
    showT = false;
    showD = false;
    showS = false;
    result: string;
    private prev = '';
    private current = '';

    constructor(private calculatorService: CalculatorService) {
      this.result = '';
    }

    pushValue(event: string): void {
      if (this.result !== '') {
        this.prev = this.current;
        this.current = event;
      }

      if (event === 'AC') {
        this.result = '';
      } else if (event === 'DEL') {
        this.result = this.prev !== '=' ? this.result.slice(0, -1) :
        this.result;
      } else if (event === '=') {
        this.result = this.calculatorService.calculate(this.result);
      } else {
        if (this.result === 'Syntax Error') {
          this.result = '' + event;
        } else {
          this.result += event;
        }
      }
    }
}
