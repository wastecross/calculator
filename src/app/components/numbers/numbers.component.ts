import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {

  @Output() sendNumber = new EventEmitter();

  addToTextBox(num: string) {
    this.sendNumber.emit(num);
  }
}
