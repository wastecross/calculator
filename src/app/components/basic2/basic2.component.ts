import { Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-basic2',
  templateUrl: './basic2.component.html',
  styleUrls: ['./basic2.component.css']
})
export class Basic2Component {
  @Output() sendValue2 = new EventEmitter();

  addToTextBox(symbol: string) {
    this.sendValue2.emit(symbol);
  }
}
