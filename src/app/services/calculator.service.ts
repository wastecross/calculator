import { Injectable } from '@angular/core';
import { SumService } from './sum.service';
import { AdditionService } from './basic/addition.service';
import { SubtractionService } from './basic/subtraction.service';
import { MultiplicationService } from './basic/multiplication.service';
import { DivitionService } from './basic/divition.service';
import { PorcentageService } from './basic/porcentage.serivce';
import { PiService } from './trigonometry/pi.service';
import { EulerService } from './scientific/euler.service';
import { ConcatService } from './concat.service';
import { TrigonometryService } from './trigonometry/trigonometry.service';


@Injectable({
    providedIn: 'root',
})

export class CalculatorService {
    result: any;
    message: string;
    temporal: object;

    constructor(
        private sumService: SumService,
        private concatService: ConcatService,
        private additionService: AdditionService,
        private subtractionService: SubtractionService,
        private multiplicationService: MultiplicationService,
        private divitionService: DivitionService,
        private porcentageService: PorcentageService,
        private piService: PiService,
        private eulerService: EulerService,
        private trigonometryService: TrigonometryService
    ) {}

    calculate(exp: string) {
        const arr = exp.split('');
        let temp: string;

        for (let i = 0; i < arr.length; i++) {
            switch (arr[i]) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    if (i !== 0) {
                        temp += arr[i];
                        arr.splice(i - 1, 2);
                        arr.unshift( temp );
                        i = 0;
                    } else {
                        temp = arr[i];
                    }
                    break;
                case '+':
                    this.temporal = this.concatService.concatTrigonometry(arr, i + 1);
                    this.temporal = this.sumService.sum(arr, i);
                    this.result = this.additionService.add(arr[i - 1], arr[i + 1]);
                    arr.splice( i - 1, 3 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case '-':
                    if (i === 0) {
                        temp = arr[i] + arr[i + 1];
                        arr.splice(i, 2);
                        arr.unshift ( temp );
                        i = 0;
                    } else {
                        this.temporal = this.concatService.concatTrigonometry(arr, i + 1);
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.subtractionService.sub(arr[i - 1], arr[i + 1]);
                        arr.splice( i - 1, 3 );
                        arr.unshift( this.result );
                        i = 0;
                    }
                    break;
                case 'x':
                    this.temporal = this.concatService.concatTrigonometry(arr, i + 1);
                    this.temporal = this.sumService.sum(arr, i);
                    this.result = this.multiplicationService.mult(arr[i - 1], arr[i + 1]);
                    arr.splice( i - 1, 3 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case '/':
                    this.temporal = this.concatService.concatTrigonometry(arr, i + 1);
                    this.temporal = this.sumService.sum(arr, i);
                    this.result = this.divitionService.div(arr[i - 1], arr[i + 1]);
                    arr.splice( i - 1, 3 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case '%':
                    this.temporal = this.concatService.concatTrigonometry(arr, i + 1);
                    this.result = this.porcentageService.porcentage(arr[i - 1]);
                    arr.splice( i - 1, 2 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case 'π':
                    this.result = this.piService.pi();
                    arr.splice( i, 1 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case 'e':
                    this.result = this.eulerService.e();
                    arr.splice( i, 1 );
                    arr.unshift( this.result );
                    i = 0;
                    break;
                case 's':
                case 'c':
                case 't':
                case 'a':
                    this.temporal = this.concatService.concat(arr, i);
                    if (arr[0] === 'sin') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.sin(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'cos') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.cos(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'tan') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.tan(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'cot') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.cot(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'sec') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.sec(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'cosec') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.cosec(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'arcsin') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.arcsin(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'arccos') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.arccos(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    } else if (arr[0] === 'arctan') {
                        this.temporal = this.sumService.sum(arr, i);
                        this.result = this.trigonometryService.arctan(arr[i + 1]);
                        arr.splice( i, 2 );
                        arr.unshift( this.result );
                    }
                    i = 0;
                    break;
                default: this.message = 'Syntax Error';
            }
        }
        return this.result === undefined ? this.message : this.result;
    }
}
