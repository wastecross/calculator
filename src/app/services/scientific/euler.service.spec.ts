import { EulerService } from './euler.service';

describe('e', () => {
  const serviceEuler = new EulerService();

  it('should return value of euler', () => {
    expect(serviceEuler.e()).toBe('2.7183');
  });
});
