import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class EulerService {
    e() {
        return Math.E.toFixed(4);
    }
}
