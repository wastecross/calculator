import { Injectable } from '@angular/core';
import { PiService } from './trigonometry/pi.service';
import { EulerService } from './scientific/euler.service';

@Injectable({
    providedIn: 'root',
})

export class SumService {
    private temp: any;

    constructor(
        private piService: PiService,
        private eulerService: EulerService
    ) {}

    sum(operation: string[], num: number): string[] {
        const init = num + 1;

        for (let i = init; i < operation.length; i++) {
            switch (operation[i]) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    if (i !== init) {
                        this.temp += operation[i];
                        operation.splice(i - 1, 2, this.temp);
                        i = num + 1;
                    } else {
                        this.temp = operation[i];
                    }
                    break;
                case '-':
                    if (i === init) {
                        this.temp = operation[i];
                    } else {
                        i = operation.length;
                    }
                    break;
                case 'π':
                    this.temp = this.piService.pi();
                    operation.splice( i, 1, this.temp);
                    break;
                case 'e':
                    this.temp = this.eulerService.e();
                    operation.splice( i, 1, this.temp);
                    break;
                default: i = operation.length;
            }
        }
        return this.temp;
    }
}
