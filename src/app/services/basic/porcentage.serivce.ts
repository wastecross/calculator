import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class PorcentageService {
    porcentage(n: string): number | string {
        const number0 = Number(n);

        return number0 === undefined ? 'Syntax Error' :
        !Number.isFinite(number0 * .01) ? 'Syntax Error' :
        number0 * .01;
    }
}
