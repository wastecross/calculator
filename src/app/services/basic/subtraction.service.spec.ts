import { SubtractionService } from './subtraction.service';

describe('sub', () => {
    const serviceSubtraction = new SubtractionService();

    it('should return the subtraction from two numbers: 10 and -5', () => {
        expect(serviceSubtraction.sub('10', '-5')).toBe(15);
    });

    it('should return the subtraction from: 4 - 10 = -6', () => {
        expect(serviceSubtraction.sub('4', '10')).toBe(-6);
    });

    it('should return the subtraction from: -20 - 13 = -33', () => {
        expect(serviceSubtraction.sub('-20', '13')).toBe(-33);
    });

    it('should return Syntax Error', () => {
        expect(serviceSubtraction.sub(undefined, undefined)).toBe('Syntax Error');
        expect(serviceSubtraction.sub(undefined, '10')).toBe('Syntax Error');
        expect(serviceSubtraction.sub('4', '10s')).toBe('Syntax Error');
        expect(serviceSubtraction.sub('10.4-9', '1')).toBe('Syntax Error');
    });
});
