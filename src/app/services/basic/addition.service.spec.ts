import { AdditionService } from './addition.service';

describe('add', () => {
    const serviceAdd = new AdditionService();

    it('should return the addition from two numbers: 5 and 6', () => {
      expect(serviceAdd.add('5', '6')).toBe(11);
    });

    it('should return the addition from two numbers: 20 and 13', () => {
      expect(serviceAdd.add('20', '13')).toBe(33);
    });

    it('should return the addition from two numbers: -4 and -10', () => {
      expect(serviceAdd.add('-4', '-10')).toBe(-14);
    });

    it('should return the addition from two numbers: -7 and 11', () => {
      expect(serviceAdd.add('-7', '11')).toBe(4);
    });

    it('should return a message: Syntax Error', () => {
      expect(serviceAdd.add(undefined, undefined)).toBe('Syntax Error');
      expect(serviceAdd.add('10', undefined)).toBe('Syntax Error');
      expect(serviceAdd.add('h', '10')).toBe('Syntax Error');
      expect(serviceAdd.add('9', '10+g')).toBe('Syntax Error');
    });
});
