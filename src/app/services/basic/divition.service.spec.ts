import { DivitionService } from './divition.service';

describe('div', () => {
    const serviceDivition = new DivitionService();

    it('should return the divition of 48 / 6: 8', () => {
      expect(serviceDivition.div('48', '6')).toBe(8);
    });

    it('should return the divition of 560 / 48: 11.666666666666666', () => {
      expect(serviceDivition.div('560', '48')).toBe(11.6666666666666666);
    });

    it('should return the divition of 380 / -8: -47.5', () => {
      expect(serviceDivition.div('380', '-8')).toBe(-47.5);
    });

    it('should return a message: Syntax Error', () => {
      expect(serviceDivition.div(undefined, undefined)).toBe('Syntax Error');
      expect(serviceDivition.div('10', undefined)).toBe('Syntax Error');
      expect(serviceDivition.div('a', '20')).toBe('Syntax Error');
      expect(serviceDivition.div('94', '10+4')).toBe('Syntax Error');
    });

    it('should return a message: Math Error', () => {
      expect(serviceDivition.div('0', '0')).toBe('Math Error');
    });
});
