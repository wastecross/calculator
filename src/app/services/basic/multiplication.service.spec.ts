import { MultiplicationService } from './multiplication.service';
import { IterableDiffers } from '@angular/core';

describe('mult', () => {
    const serviceMult = new MultiplicationService();

    it('should return the multiplication of 4 * -10 = -40', () => {
      expect(serviceMult.mult('4', '-10')).toBe(-40);
    });

    it('should return the multiplication of -92 * -4= 368', () => {
      expect(serviceMult.mult('-92', '-4')).toBe(368);
    });

    it('should return the multiplication of 21 * 190 = 3990', () => {
      expect(serviceMult.mult('21', '190')).toBe(3990);
    });

    it('should return a message: Syntax Error', () => {
      expect(serviceMult.mult(undefined, undefined)).toBe('Syntax Error');
      expect(serviceMult.mult('20', undefined)).toBe('Syntax Error');
      expect(serviceMult.mult('7', 's')).toBe('Syntax Error');
      expect(serviceMult.mult('8-9', '10')).toBe('Syntax Error');
    });
});
