import { PorcentageService } from './porcentage.serivce';

describe('porcentage', () => {
    const servicePorcentage = new PorcentageService();

    it('should return the porcentage of 10 = 0.1', () => {
      expect(servicePorcentage.porcentage('10')).toBe(0.1);
    });

    it('should return the porcentage of 75 = 0.75', () => {
      expect(servicePorcentage.porcentage('75')).toBe(0.75);
    });

    it('should return the porcentage of -96% = -0.96', () => {
      expect(servicePorcentage.porcentage('-96')).toBe(-0.96);
    });

    it('should return message: Syntax Error', () => {
    expect(servicePorcentage.porcentage(undefined)).toBe('Syntax Error');
    expect(servicePorcentage.porcentage('9ol')).toBe('Syntax Error');
    });
});
