import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class SubtractionService {
    sub(n1: string, n2: string): number | string {
        const number1 = Number( n1 );
        const number2 = Number( n2 );

        return number1 === undefined ? 'Syntax Error' :
        number2 === undefined ? 'Syntax Error' :
        !Number.isFinite(number1 - number2) ? 'Syntax Error' :
        number1 - number2;
    }
}
