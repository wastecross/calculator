import { Injectable } from '@angular/core';
import { SumService } from './sum.service';
import { TrigonometryService } from './trigonometry/trigonometry.service';

@Injectable({
    providedIn: 'root',
})

export class ConcatService {
    private temp: any = [];

    constructor(
        private sumService: SumService,
        private trigonometryService: TrigonometryService
    ) {}

    concat(array: string[], num: number): string[] {
        for (let i = num; i < array.length; i++) {
            switch (array[i]) {
                case 's':
                case 'i':
                case 'n':
                case 'c':
                case 'o':
                case 't':
                case 'a':
                case 'e':
                case 'r':
                    if ( i !== num) {
                        this.temp += array[i];
                        array.splice(i - 1, 2, this.temp);
                        i = num;
                    } else {
                        this.temp = array[i];
                    }
                    break;
                default: i = array.length;
            }
        }
        return this.temp;
    }

    concatTrigonometry(array: string[], num: number): string[] {
        for (let i = num; i < array.length; i++) {
            switch (array[i]) {
                case 's':
                case 'i':
                case 'n':
                case 'c':
                case 'o':
                case 't':
                case 'a':
                case 'e':
                case 'r':
                    if ( i !== num) {
                        this.temp += array[i];
                        array.splice(i - 1, 2, this.temp);
                        i = num;
                    } else {
                        this.temp = array[i];
                    }
                    break;
                default: i = array.length;
            }
            if (array[i] === 'sin') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.sin(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'cos') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.cos(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'tan') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.tan(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'cot') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.cot(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'sec') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.sec(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'cosec') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.cosec(array[i + 1]);
                array.splice( i, 2, this.temp);
            } else if (array[i] === 'arcsin') {
                this.temp = this.sumService.sum(array, i);
                this.temp = this.trigonometryService.arcsin(array[i + 1]);
                array.splice( i, 2, this.temp);
            }
        }
        return this.temp;
    }
}
