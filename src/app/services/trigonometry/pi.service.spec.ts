import { PiService } from './pi.service';

describe('pi', () => {
  const servicePI = new PiService();

  it('should return value of pi', () => {
    expect(servicePI.pi()).toBe('3.1416');
  });
});
