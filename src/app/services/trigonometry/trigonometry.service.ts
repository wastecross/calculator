import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrigonometryService {

  sin(n: string): number | string {
    const numberSin = Number(n);

    return numberSin === undefined ? 'Syntax Error' :
    !Number.isFinite(Math.sin(numberSin)) ? 'Syntax Error' :
    Math.sin(numberSin);
  }

  cos(n: string): number | string {
    const numberCos = Number(n);

    return numberCos === undefined ? 'Syntax Error' :
    !Number.isFinite(Math.cos(numberCos)) ? 'Syntax Error' :
    Math.cos(numberCos);
  }

  tan(n: string): number | string {
    const numberTan = Number(n);

    return numberTan === undefined ? 'Syntax Error' :
    !Number.isFinite(Math.tan(numberTan)) ? 'Syntax Error' :
    Math.tan(numberTan);
  }

  cot(n: string): number | string {
    const numberCot = Number(n);

    return numberCot === undefined ? 'Syntax Error' :
    Number.isNaN(numberCot) ? 'Syntax Error' :
    !Number.isFinite(1 / Math.tan(numberCot)) ? 'Math Error' :
    1 / Math.tan(numberCot);
  }

  sec(n: string): number | string {
    const numberSec = Number(n);

    return numberSec === undefined ? 'Syntax Error' :
    !Number.isFinite(1 / Math.cos(numberSec)) ? 'Syntax Error' :
    1 / Math.cos(numberSec);
  }

  cosec(n: string): number | string {
    const numberCosec = Number(n);

    return numberCosec === undefined ? 'Syntax Error' :
    Number.isNaN(numberCosec) ? 'Syntax Error' :
    !Number.isFinite(1 / Math.sin(numberCosec)) ? 'Math Error' :
    1 / Math.sin(numberCosec);
  }

  arcsin(n: string): number | string {
    const numberArcsin = Number(n);

    return numberArcsin === undefined ? 'Syntax Error' :
    Number.isNaN(numberArcsin) ? 'Syntax Error' :
    !Number.isFinite(Math.asin(numberArcsin)) ? 'Math Error' :
    Math.asin(numberArcsin);
  }

  arccos(n: string): number | string {
    const numberArccos = Number(n);

    return numberArccos === undefined ? 'Syntax Error' :
    Number.isNaN(numberArccos) ? 'Syntax Error' :
    !Number.isFinite(Math.acos(numberArccos)) ? 'Math Error' :
    Math.acos(numberArccos);
  }

  arctan(n: string): number | string {
    const numberArctan = Number(n);

    return numberArctan === undefined ? 'Syntax Error' :
    !Number.isFinite(Math.atan(numberArctan)) ? 'Syntax Error' :
    Math.atan(numberArctan);
  }
}
