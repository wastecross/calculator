import { TrigonometryService } from './trigonometry.service';

const serviceTri = new TrigonometryService();

describe('sin', () => {
  it('should return sin of 4: -0.7568024953079282', () => {
    expect(serviceTri.sin('4')).toBe(-0.7568024953079282);
  });

  it('should return sin of -10: 0.5440211108893698', () => {
    expect(serviceTri.sin('-10')).toBe(0.5440211108893698);
  });

  it('should return sin of 0: 0', () => {
    expect(serviceTri.sin('0')).toBe(0);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.sin(undefined)).toBe('Syntax Error');
    expect(serviceTri.sin('9*g')).toBe('Syntax Error');
    expect(serviceTri.sin('9*9')).toBe('Syntax Error');
  });
});

describe('cos', () => {
  it('should return cos of 10: -0.8390715290764524', () => {
    expect(serviceTri.cos('10')).toBe(-0.8390715290764524);
  });

  it('should return cos of 0: 1', () => {
    expect(serviceTri.cos('0')).toBe(1);
  });

  it('should return cos of -90: -0.4480736161291702', () => {
    expect(serviceTri.cos('-90')).toBe(-0.4480736161291702);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.cos(undefined)).toBe('Syntax Error');
    expect(serviceTri.cos('a4')).toBe('Syntax Error');
    expect(serviceTri.cos('9+10')).toBe('Syntax Error');
  });
});

describe('tan', () => {
  it('should return tan of 0: 0', () => {
    expect(serviceTri.tan('0')).toBe(0);
  });

  it('should return tan of 120: 0.7131230097859091', () => {
    expect(serviceTri.tan('120')).toBe(0.7131230097859091);
  });

  it('should return tan of -45: -1.6197751905438615', () => {
    expect(serviceTri.tan('-45')).toBe(-1.6197751905438615);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.tan(undefined)).toBe('Syntax Error');
    expect(serviceTri.tan('A')).toBe('Syntax Error');
    expect(serviceTri.tan('10+4')).toBe('Syntax Error');
  });
});

describe('cot', () => {
  it('should return cot of 1: 0.6420926159343306', () => {
    expect(serviceTri.cot('1')).toBe(0.6420926159343306);
  });

  it('should return cot of 45: 0.6173696237835551', () => {
    expect(serviceTri.cot('45')).toBe(0.6173696237835551);
  });

  it('should return cot of -10: -1.5423510453569202', () => {
    expect(serviceTri.cot('-10')).toBe(-1.5423510453569202);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.cot(undefined)).toBe('Syntax Error');
  });

  it('should return Math Error', () => {
    expect(serviceTri.cot('0')).toBe('Math Error');
  });
});

describe('sec', () => {
  it('should return sec of 10: -1.1917935066878957', () => {
    expect(serviceTri.sec('10')).toBe(-1.1917935066878957);
  });

  it('should return sec of -20: 2.4504875209567056', () => {
    expect(serviceTri.sec('20')).toBe(2.4504875209567056);
  });

  it('should return sec of 0: 1', () => {
    expect(serviceTri.sec('0')).toBe(1);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.sec(undefined)).toBe('Syntax Error');
    expect(serviceTri.sec('9+0')).toBe('Syntax Error');
  });
});

describe('cosec', () => {
  it('should return cosec of 90: 1.1185724071637084', () => {
    expect(serviceTri.cosec('90')).toBe(1.1185724071637084);
  });

  it('should return cosec of -45: -1.175221363135749', () => {
    expect(serviceTri.cosec('-45')).toBe(-1.175221363135749);
  });

  it('should return cosec of 15.2: 2.055926594614191', () => {
    expect(serviceTri.cosec('15.2')).toBe(2.055926594614191);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.cosec(undefined)).toBe('Syntax Error');
    expect(serviceTri.cosec('10+x')).toBe('Syntax Error');
  });
});

describe('arcsin', () => {
  it('sould return arcsin of 1: 1.5707963267948966', () => {
    expect(serviceTri.arcsin('1')).toBe(1.5707963267948966);
  });

  it('should return arcsn of -1: -1.5707963267948966', () => {
    expect(serviceTri.arcsin('-1')).toBe(-1.5707963267948966);
  });

  it('shoul return arcsin of -0.5: -0.5235987755982989', () => {
    expect(serviceTri.arcsin('-0.5')).toBe(-0.5235987755982989);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.arcsin(undefined)).toBe('Syntax Error');
  });

  it('should return Math Error', () => {
    expect(serviceTri.arcsin('2')).toBe('Math Error');
  });
});

describe('arccos', () => {
  it('sould return arccos of 1: 0', () => {
    expect(serviceTri.arccos('1')).toBe(0);
  });

  it('should return arccos of -1: 3.141592653589793', () => {
    expect(serviceTri.arccos('-1')).toBe(3.141592653589793);
  });

  it('shoul return arccos of -0.5: 2.0943951023931957', () => {
    expect(serviceTri.arccos('-0.5')).toBe(2.0943951023931957);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.arccos(undefined)).toBe('Syntax Error');
  });

  it('should return Math Error', () => {
    expect(serviceTri.arccos('2')).toBe('Math Error');
  });
});

describe('arctan', () => {
  it('should return arctan of 1: 0.7853981633974483', () => {
    expect(serviceTri.arctan('1')).toBe(0.7853981633974483);
  });

  it('should return arctan of 10: 1.4711276743037347', () => {
    expect(serviceTri.arctan('10')).toBe(1.4711276743037347);
  });

  it('should return arctan of -20: -1.5208379310729538', () => {
    expect(serviceTri.arctan('-20')).toBe(-1.5208379310729538);
  });

  it('should return Syntax Error', () => {
    expect(serviceTri.arctan(undefined)).toBe('Syntax Error');
  });
});
