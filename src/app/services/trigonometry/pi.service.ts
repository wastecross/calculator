import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class PiService {
  pi() {
    return Math.PI.toFixed(4);
  }
}
